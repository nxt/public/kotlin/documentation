# A brief history of Kotlin

## Who's behind Kotlin?

A company called **Jetbrains**.

Software Company from Prague, Czech Republic

Famous for IDE's like JetBrains, ReSharper, WebStorm, ...

## Chronology

* 2011, July: [Announcement of Kotlin][hello_world]
* 2012, February: [Open Source Release][open_source]
* 2016, February: [Kotlin 1.0][kotlin_1] with Backwards Compatibility Guarantee
* 2017, January: [Kotlin support in Spring Framework 5.0][spring]
* 2017, May: [First-class support for Kotlin on Android][android]
* 2019, May: ["Kotlin-first" on Android][kotlin_first]

[hello_world]: https://blog.jetbrains.com/kotlin/2011/07/hello-world-2/
[open_source]: https://blog.jetbrains.com/kotlin/2012/02/kotlin-goes-open-source-2/
[kotlin_1]: https://blog.jetbrains.com/kotlin/2016/02/kotlin-1-0-released-pragmatic-language-for-jvm-and-android/
[spring]: https://spring.io/blog/2017/01/04/introducing-kotlin-support-in-spring-framework-5-0
[android]: https://youtu.be/EtQ8Le8-zyo?t=562
[kotlin_first]: https://youtu.be/LoLqSbV1ELU?t=460

## Motivation

* Fast, Easy to Learn, Easy to Use
* Powerful
* Compiles as fast as Java
* JVM / Java interoperability
* Drive Sales of IntelliJ

## Basics

https://kotlinlang.org

- Downloads
- Documentation (!!)
- Community (Slack, etc.)
