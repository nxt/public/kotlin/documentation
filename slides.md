---
title: Kotlin Workshop
author:
  - Christian Mäder
institute:
  - nxt Engineering GmbH
date: 2019-09-10

# html slides
lang: en-CH
revealjs-url: https://revealjs.com

# latex/pdf slides
fontfamily: roboto
header-includes:
- |
  ```{=latex}
  \definecolor{main}{RGB}{0, 61, 113}
  \definecolor{background}{RGB}{236, 240, 241}
  ```
---

!include chapter0/index.md

!include chapter1/index.md

!include chapter2/index.md

!include chapter3/index.md

!include chapter4/index.md

<!-- !include chapter5/index.md -->

!include contact.md
