# Advantages of Kotlin

## Specific Kotlin Functionality

* Operator Overloading
* Extensions Functions
* Co-Routines
* Kotlin/JS and Kotlin/Native

## Pro Kotlin (Code)

- Similar to Java
- Compatible with JVM/Java
- A Modern Syntax with little boilerplate
- Null-safety through the type system (Less Checks, Almost no NPE)
- Enhanced standard library

## Pro Kotlin (Ecosystem)

* Excellent tooling support
* Excellent documentation
* Big community
* Strong industry support

## Con Kotlin (cons)

* Java knowledge still recommended
* Some code must be explicitly made "compatible" with Java
* New idioms
* Language features that don't exist in Java
* Bigger "binaries" because of the Kotlin Stdlib
* JVM licensing

## What Kotlin says

https://kotlinlang.org/docs/reference/comparison-to-java.html
