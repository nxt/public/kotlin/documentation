# Kotlin Basics

## Packages

### Java & Kotlin

```{.kotlin .numberLines}
package engineering.nxt.kotlin_everywhere

import java.util.*
```

## Functions

### Java

```{.java .numberLines}
class JavaClass {
  public static long sum(long a, long b) {
    return a + b;
  }
}
```

## Functions

* Input: `file.kt`
* Output: `FileKt.class`

### Kotlin

```{.kotlin .numberLines}
fun sum(a: Long, b: Long): Long {
  return a + b
}
```

### Syntactic Sugar

```{.kotlin .numberLines}
fun sum(a: Long, b: Long) = a + b
```

## Functions (return void)

### Java

```{.java .numberLines}
class JavaClass {
  public static void printSum(long a, long b) {
    long sum = a + b;
    System.out.println("a + b = " + sum);
  }
}
```

## Functions (return void)

### Kotlin

```{.kotlin .numberLines}
fun printSum(a: Long, b: Long) {
  var sum = a + b
  println("a + b = $sum")
}
```

### Full Definition

```{.kotlin .numberLines}
fun printSum(a: Long, b: Long): Unit {
  // ...
}
```

## Variables and Values

### Variables: Read & Write

```{.kotlin .numberLines}
var a: Long = 1
var b = 2L // Long inferred
var c: Long
c = 3
c += 1
```

## Variables and Values

### Values: Readonly

```{.kotlin .numberLines}
val a: Long = 1
val b = 2L // Long inferred
val c: Long
c = 3
c += 1 // error: val cannot be reassigned
```

## Strings

### Java

```{.java .numberLines}
String line1 = "We will";
String line2 = "Rock you!";

System.out.println(
  line1 + " " + line1 + " " + line2);
```

### Kotlin

```{.kotlin .numberLines}
val line1 = "We will"
val line2 = "Rock you!"

println("${line1} ${line1} ${line2}")
println("$line1 $line1 $line2")
```

## Conditional Expression

### Java

```{.java .numberLines}
public static void main(String[] args) {
  Random r = new java.util.Random();
  Integer i = r.nextInt();
  Integer j = r.nextInt();

  System.out.println(
    "min(" + i + "," + j + ") = " + min(i, j));
}

public static Integer min(Integer i, Integer j) {
  if (i<j) return i;
  else     return j;
}
```

## Conditional Expression

### Kotlin (A)

```{.kotlin .numberLines}
fun main() {
  val r = java.util.Random() // no new
  val i = r.nextInt()
  val j = r.nextInt()

  println("min($i,$j) = ${min(i,j)}")
}

fun min(i: Int, j: Int): Int {
  if (i<j) return i
  else     return j
}
```

## Conditional Expression

### Kotlin (B)

```{.kotlin .numberLines}
fun main() {
    val i = java.util.Random().nextInt()
    val j = java.util.Random().nextInt()

    println("min(${i},${j})=${min(i,j)}")
}

fun min(i: Int, j: Int): Int {
  return if (i<j) i else j
}
```

## Conditional Expression

### Kotlin (C)

```{.kotlin .numberLines}
fun main() {
    val i = java.util.Random().nextInt()
    val j = java.util.Random().nextInt()

    println("min(${i},${j})=${min(i,j)}")
}

fun min(i: Int, j: Int) = if (i<j) i else j
```

## For & While Loop

### Java

```{.java .numberLines}
Iterable<?> items = // ...
for (Object item : items) {
  System.out.println(item);
}
```

### Kotlin

```{.kotlin .numberLines}
Iterable<Any> items = // ...
for (item in items) {
  println(i)
}
```

## For & While Loop

### Java

```{.java .numberLines}
Iterable<?> items = // ...
for (int i = 0; i < items.size(); i++) {
  System.out.println(i + " is " + items.get(i);
}
```

### Kotlin

```{.kotlin .numberLines}
Iterable<Any> items = // ...
for (i in items.indices) {
  println("$i is ${items[i]}")
}
```

## For & While Loop

### Java

```{.java .numberLines}
Iterable<?> items = // ...
for (int i = 0; i < items.size(); i++) {
  System.out.println(i + " is " + items.get(i);
}
```

### Kotlin (Alternative)

```{.kotlin .numberLines}
Iterable<Any> items = // ...
for ((index, value) in items.withIndex()) {
  println("$index is $value")
}
```

## Range Expressions

Kotlin has range expressions which can be quite handy.

### Java

```{.java .numberLines}
for (int i = 0; i <= 100; i++) {
  System.out.println(i);
}
```

### Kotlin

```{.kotlin .numberLines}
for (i in 0..100) {
  println(i)
}
```

## Range Expressions (Step & Downwards)

### Java

```{.java .numberLines}
for (int i = 0; i <= 100; i += 2)
  System.out.println(i);
for (int i = 100; i >= 0; i -= 2)
  System.out.println(i);
```

### Kotlin

```{.kotlin .numberLines}
for (i in 0..100 step 2)
  println(i)
for (i in 100 downTo 1 step 2)
  println(i)
```

## For & While Loop

### Java

```{.java .numberLines}
while (something.isNotOver()) {
  something.do();
}
```

### Kotlin

```{.kotlin .numberLines}
while (something.isNotOver()) {
  something.do()
}
```

## For & While Loop

### Java

```{.java .numberLines}
do {
  something.do();
} while (something.isNotOver())
```

### Kotlin

```{.kotlin .numberLines}
do {
  something.do()
} while (something.isNotOver())
```

## When Expression

### Java

```{.java .numberLines}
switch (i) {
  case 0:
  case 1:
    System.out.println("0 or 1");
    break;
  default:
    System.out.println("not 0 or 1");
}
```

## When Expression

### Kotlin

```{.kotlin .numberLines}
when (i) {
  0, 1 -> println("0 or 1")
  else -> println("not 0 or 1")
}
```

## When Expression

### Java

```{.java .numberLines}
if (i >= 0 && i <= 9) {
  System.out.println("Easy")
} else if (i >= 10 && i <= 99) {
  System.out.println("Medium")
} else if (i >= 100 && i <= 999) {
  System.out.println("Hard")
} else {
  throw new IllegalArgumentException(
    "Value is not in range 0..999")
}
```

## When Expression

### Kotlin

```{.kotlin .numberLines}
when (i) {
  in 0..9 -> println("Easy")
  in 10..99 -> println("Medium")
  in 100..999 -> println("Hard")
  else -> throw IllegalArgumentException(
    "Value is not in range 0..999")
}
```

## Exercise 1

Install Kotlin

### Kotlin

```{.kotlin .numberLines}
// Filename: hello.kt
fun main() = println("Hello World")
```

### Compile and Build

```bash
kotlinc hello.kt
kotlin HelloKt
```

## Nullability

* Kotlin is a Null-Safe language.
* Types are either _nullable_ or _non-nullable_.
* Objects from the Java world are _nullable_ by default.

## Nullability: Initialization and Assignment

### Java

```{.java .numberLines}
class Session {
  private String cookie;
  String login() {
    if(cookie==null) {cookie="very_random"}
    return cookie;
  }
  void logout() {
    cookie = null;
  }
  boolean isLoggedIn() {
    return cookie != null;
  }
}
```

## Nullability: Initialization and Assignment

### Kotlin

```{.kotlin .numberLines}
class Session {
  private var cookie: String // won't compile
  fun login(): String {
    if (cookie == null) {cookie = "very_random"}
    return cookie
  }
  fun logout() {
    cookie = null
  }
  fun isLoggedIn() = cookie != null
}
```

## Nullability: Initialization and Assignment

### Kotlin

```{.kotlin .numberLines}
class Session {
  private var cookie: String // Must initialize
  fun login(): String {
    if (cookie == null) {cookie = "very_random"}
    return cookie
  }
  fun logout() {
    cookie = null // Error: null not allowed
  }
  fun isLoggedIn() = cookie != null
}
```

## Nullability: Initialization and Assignment

### Kotlin

```{.kotlin .numberLines}
class Session {
  private var cookie: String? = null // explicit
  fun login(): String {   // mustn't return null
    if (cookie == null) {cookie = "very_random"}
    return cookie
  }
  fun logout() {
    cookie = null // now ok
  }
  fun isLoggedIn() = cookie != null
}
```

## Nullability: Initialization and Assignment

### Kotlin

```{.kotlin .numberLines}
class Session {
  private var cookie: String? = null
  fun login(): String {
    if (cookie == null) {cookie = "very_random"}
    return cookie!!     // could already be null
  }
  fun logout() {
    cookie = null
  }
  fun isLoggedIn() = cookie != null
}
```

## Nullability: Accessing Instances of Nullables

### Kotlin

```{.kotlin .numberLines}
import java.util.Random
import java.lang.StringBuilder

fun mayBeNull(): String? =
  if (Random().nextInt() % 2 == 0) "I'm not null"
  else null

fun printLength(s: String) {
  val len = s.length
  println("The input's length is $len")
}

fun main() {
  // next slide
}
```

## Nullability: Accessing Instances of Nullables

### Kotlin

```{.kotlin .numberLines}
fun mayBeNull(): String? = // ...
fun printLength(s: String) = // ...

fun main() {
  val couldBeNull: String? = mayBeNull()

  // What will the error be?
  // Why?
  printLength( couldBeNull )
}
```

## Nullability: Accessing Instances of Nullables

### Kotlin

```{.kotlin .numberLines}
fun mayBeNull(): String? = // ...
fun printLength(s: String) = // ...

fun main() {
  val couldBeNull: String? = mayBeNull()

  // error: type mismatch:
  // inferred type is String? but
  // String was expected
  printLength( couldBeNull )
}
```

## Nullability: Accessing Instances of Nullables

### Kotlin

```{.kotlin .numberLines}
fun mayBeNull(): String? = // ...
fun printLength(s: String) = // ...

fun main() {
  val couldBeNull: String? = mayBeNull()

  if (couldBeNull == null) { // null check
    return
  }

  val result = printLength(couldBeNull) // ok
}
```

## Nullability: Accessing Instances of Nullables

### Kotlin

```{.kotlin .numberLines}
fun mayBeNull(): String? = // ...

// change type
fun printLength(s: String?) {

  // safe call operator
  val len = s?.length;
  //      = s != null ? s.length : null

  println("The input's length is $len")
}

fun main() = // ...
```

## Nullability: Accessing Instances of Nullables

### Kotlin

```{.kotlin .numberLines}
fun mayBeNull(): String? = // ...

fun printLength(s: String?) {
  val len = s?.length; // number or null

  // Elvis Operator
  val printLen = len ?: -1;
  //           = len != null ? len : -1

  println("The input's length is $len")
}

fun main() = // ...
```

## Nullability: Accessing Instances of Nullables

### Kotlin

```{.kotlin .numberLines}
fun actuallyNotNull(): String? =
  if (Random().nextInt() % 1 == 0) "I'm not null"
  else null

fun printLength(s: String) = // no "?"
  println("The input's length is ${s.length}")

fun main() {
  val shouldntBeNull: String? = actuallyNotNull()

  // the not-null assertion operator compiles,
  // but throws an NPE if shouldntBeNull is null
  val result = printLength(shouldntBeNull!!)
}
```

## Smart Casts

Also called _Automatic Casts_.

### Java

```{.java .numberLines}
public void sizePrinter(T foo) {
  if (foo instanceof String) {
    String fooStr = (String)foo;
    System.out.println("The length is " + fooStr.length)
  }
}
```

## Smart Casts

Also called _Automatic Casts_.

### Kotlin

```{.kotlin .numberLines}
fun sizePrinter(T foo) {
  if (foo is String) {
    // foo is now a String
    println("The length is ${foo.length}")
  }
}
```

## Smart Casts (2)

### Kotlin

```{.kotlin .numberLines}
fun sizePrinter(T foo) {
  if (foo !is String) {
    return
  }

  // foo is now a String
  println("The length is ${foo.length}")
}
```

## Lambdas

### Java

```{.java .numberLines}
int[] numbers = new int[] {0,1,2,3,4,5,6,7,8,9}
int sumOfEvenNumbers = Arrays.stream(numbers)
                        .filter(x -> x % 2 == 0)
                        .sum()
```

### Kotlin

```{.kotlin .numberLines}
val numbers = 0..9
val sumOfEvenNumbers = numbers
                    .filter({ x -> x % 2 == 0 })
                    .sum()
```

## Lambdas

```{.kotlin .numberLines}
val numbers = 0..9

numbers.filter({ x -> x % 2 == 0 }).sum()
numbers.filter{ x -> x % 2 == 0 }.sum()
numbers.filter{ it % 2 == 0 }.sum()

val filterA = { x: Int -> x % s == 0 }
numbers.filter(filterA)

val filterB: (Int) -> Boolean = { it % 2 == 0}
numbers.filter(filterB)
```

## Exercise 2

![](chapter2/rock_paper_scissors.jpg)
