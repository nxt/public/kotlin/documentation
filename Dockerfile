FROM dalibo/pandocker

RUN wget -qO - https://github.com/owickstrom/pandoc-include-code/releases/download/v1.2.0.2/pandoc-include-code-linux-ghc8-pandoc-1-19.tar.gz \
  | tar xzf - \
 && mv pandoc-include-code /usr/bin/

RUN apt-get update \
 && apt-get install -y fonts-roboto \
 && rm -rf /var/lib/apt/lists/*

RUN tlmgr install \
      fira \
      appendixnumberbeamer \
      pgf \
      roboto \
      beamertheme-focus \
      beamertheme-metropolis
