---
title: Kotlin für Java Entwickler
author:
  - Christian Mäder
institute:
  - nxt Engineering GmbH
date: 2019-09-10
documentclass: article
papersize: a4
fontfamily: roboto
mainfont: Roboto Regular
links-as-notes: true
---

# Kotlin für Java Entwickler

Seitdem Kotlin von Jetbrains veröffentlich wurde, haben viele Entwickler weltweit diese Programmiersprache als moderne Alternative zu Java entdeckt.
Unter Android ist seit der Google Developer Conference vom Mai 2019 die Devise sogar "Kotlin First".

Kotlin bietet eine aufgeräumte Syntax, die kompatibel zu Java und allen weiteren JVM Sprachen ist.
Da Kotlin sich perfekt in den bekanntesten Build-Systemen für Java, Maven und Gradle, integriert, kann Kotlin einfach in ein bestehendes Java Projekt eingeführt werden, ohne das ganze Projekt neu entwickeln zu müssen.

## Programm

1) A brief history of Kotlin
1) Kotlin Grundlagen (Grundlegende Syntax, Typen, Compiler)
1) Kotlin loves Java
1) Erweitere Kotlin Funktionen (Co-Routines, Extension Functions, )
1) Exkurs nach Kotlin/* (Kotlin/Native, Kotlin/JS, Kotlin on Android, ...)

## Kursziel

Die Kursteilnehmer wissen am Ende des Kurses:

* Wie sie neues Kotlin-Projekt starten
* Wie sie Programme in Kotlin schreiben
* Wie sie ein bestehendes Java-Projekt (Maven/Gradle) um Kotlin erweitern
* Wie sie sich selbstständig Hilfe zu Kotlin holen können
* Wie sie sich selbstständig in weitere Kotlin Themen einarbeiten können

## Zielgruppe

Dieser Kurs richtet sich an Java-Entwickler, die interessiert sind, Kotlin kennenzulernen.
Der Schwerpunkt dieses Kurses sind Web-basierte Applikationen und wie von Java auf Kotlin umgestiegen werden kann.
Vom Kurs können auch Android-Entwickler profitieren, die von Java auf Kotlin wechseln möchten. Auch wenn der Schwerpunkt nicht Android ist, gelten praktisch die gleichen Prinzipien wie in der "normalen" Java-Welt.

## Voraussetzungen

Die Teilnehmer sollten solide Java-Kenntnisse mitbringen. Zudem Freude, eine neue Programmiersprache und deren Paradigmen kennen zu lernen.

## Benötigte Infrastruktur

* Laptop (4GB RAM oder mehr, WiFi) mit Windows, Linux oder macOS
* Aktueller Firefox- oder Chrome-Browser (oder Chrome-kompatibel, zB Chromium)
* JDK 8 oder neuer
* IntelliJ Community Edition oder Ultimate Edition


!include chapter1/index.md

!include chapter2/index.md

!include chapter3/index.md

!include contact.md
