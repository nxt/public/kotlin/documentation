# Kotlin And Java

## Overview

* Kotlin code can access any Java code
* Java code can access all Kotlin code

Accessing Kotlin code from Java involves:

* Knowledge about how Kotlin compiles its code
* Might require some `@Jvm` annotations

As usual, [everything is well documented][kotlin_java].

[kotlin_java]: https://kotlinlang.org/docs/reference/java-interop.html

## Call Java Code From Kotlin

### Java

``` java
public class MyClass {
  private final String field;
  public MyClass(String s) { field = s; }
  public String getField() { return field }
}
```

### Kotlin

```{.kotlin .numberLines}
fun main() {
  val myClass = MyClass("Parameter")
  println(myClass.field) // calls 'getField()'
}
```

## Call Java Code from Kotlin (null)

Pay attention to `null` when interoperating with Java:

```{.java .numberLines}
// Java
class ListOfThings {
  private List<String> list = new ArrayList<>();
  public List<String> getList() {
    list.add(null);
    return list; } }
```

```{.kotlin .numberLines}
// Kotlin
fun main() {
  val list = ListOfThings().list
  val item: String = list.first() // compiles
  // java.lang.IllegalStateException:
  //   item must not be null
}
```

## Call Java Code from Kotlin (Nullability)

The Kotlin compiler supports annotation indicating the nullabilty:

- `@Nullable` and `@NotNull` from JetBrains
- JSR-305 `@Nonnull(when = When.ALWAYS)`
- FindBugs annotations
- Eclipse annotation
- Lombok `@NonNull` annotation

## Call Java Code from Kotlin (Primitive Types)

Java types are mapped to Kotlin types in an intuitive way:

* `int` to `kotlin.Int`
* `boolean` to `kotlin.Boolean`
* etc.

Boxed types are mapped to the nullable types in Kotlin:

* `java.lang.Integer` to `kotlin.Int?`
* `java.lang.Boolean` to `kotlin.Bool?`
* etc.

## Call Java Code from Kotlin (Arrays)

Java arrays are mapped in a similar way:

* `int[]` to `kotlin.IntArray`
* `boolean[]` to `kotlin.BooleanArray`
* etc.

There are also the `intArrayOf(1,2,3)` functions that create Java-compatible arrays.

## Call Java Code from Kotlin (Generics)

Also works very intuitively, although Kotlin's _Generic_-System is a little bit different.

## Call Java Code from Kotlin (Checked Exceptions)

Kotlin does not enforce exception checks:

```{.kotlin .numberLines}
// try(BufferedReader br = new BufferedReader(...))
BufferedReader(FileReader("file.txt")).use { br ->
  val sb = StringBuilder()
  val line = br.readLine() // throws IOException

  while (line != null) {
    line = br.readLine()
  }
}
```

## Call Java Code from Kotlin (SAM)

In Java, you can use Lambdas for **s**ingle **a**bstract **m**ethod (SAM) interfaces.
In Kotlin this is done by just passing a block:

```{.kotlin .numberLines}
import java.util.concurrent.Callable

fun main() {
  val cal = Callable { println("Hello World") }
  cal.call() // prints "Hello World"
}
```

## Call Kotlin Code from Java

Calling Kotlin code from Java is mostly intuitive as well:

```{.kotlin .numberLines}
// Kotlin
data class Thing(var name: String)
```

```{.java .numberLines}
// Java
public class Main {
  public static void main(String args...) {
    Thing w = new Thing("Water");
    System.out
      .println("The thing is " + w.getName());
  }
}
```

## Call Kotlin Code form Java (Package-level functions)

```{.kotlin .numberLines}
// hello.kt
package workshop
fun helper() = "help is on the way"
```

```{.java .numberLines}
// java
public class Foo {
  public void couldUseHelp() {
    System.out.println(
      workshop.HelloKt.helper()
    );
    // prints "help is on the way\n"
  } }
```

## Call Kotlin Code form Java (@JvmName)

```{.kotlin .numberLines}
// Kotlin
@file:JvmName("Helper")
package workshop
fun helper() = "help is on the way"
```

```{.java .numberLines}
// Java
public class Foo {
  public void couldUseHelp() {
    System.out.println(
      workshop.Helper.helper()
    );
    // prints "help is on the way\n"
  } }
```

## Call Kotlin Code form Java (Static Fields)

```{.java .numberLines}
// Java
class Item {
  public String name;
}
```

```{.kotlin .numberLines}
// Kotlin
class Item(name: String) {
  @JvmField val name = name;
}
```

## Exercise 1

Adding Kotlin to a Spring Java Project.

![](chapter3/start_spring_io.jpg)

## Exercise 1

You will eventually need IntelliJ Community or Ultimate edition.

![](chapter3/intellij_idea.jpg)
