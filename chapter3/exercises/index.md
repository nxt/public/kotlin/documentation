---
title: Kotlin Workshop — Exercises
author:
  - Christian Mäder
institute:
  - nxt Engineering GmbH
date: 2019-09-10
---

# Chapter 3

Kotlin & Java

## Exercise 1: Spring Project from Java to Kotlin

In this exercise we'll start with a trivial Java-based Spring project and use it with the Kotlin-based _Rock, Paper, Scissors_ game we've made before.

### Preparation

Go to [start.spring.io][spring_init] and choose this:

* Gradle Project
* Java
* Spring Boot 2.1.8
* Project Metadata: `engineering.nxt`
* Artifact: `kotlin_everywhere`
* Dependencies:
  * Web
  * Mustache
  * JPA
  * H2
  * DevTools

This will eventually look like that:

![start.spring.io](chapter3/start_spring_io.jpg)

[spring_init]: https://start.spring.io

### The Java App

The java app will be a very simple online version of the _Rock, Paper, Scissors_ game we've made before:

* A choice must be made by the player.
* The computer will make a choice.
* The winner will be shown.
* There will be a score counter that is persisted in the database.

The corresponding domain model is therefore really simple:

```
+----------------------------------+
| Game                             |
+----------------------------------+
| id: Number, Primary Key          |
| userChoice: String, Not Null   |
| computerChoice: String, Not Null |
| userScore: Number, Not Null    |
| computerScore: Number, Not Null  |
+----------------------------------+
```

This will track every round that is played.
When the player wins, the `userScore` will be `1` and the `computerScore` will be 0 (and vice-versa).
The total score for the player will be the sum of all `userScore` entries and the computer score will be the sum of all `computerScore` entries, respectively.

As a side-effect of this type of score-keeping we get a history of all the games played.

#### The Entity

We're going to create the domain object first.

Create a new file `src/main/java/engineering/nxt/kotlin_everywhere/Game.java`.
The file should like like this:

```{.java .numberLines}
package engineering.nxt.kotlin_everywhere;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Game {
  @Id
  @GeneratedValue
  private Long id;
  private String userChoice;
  private String computerChoice;
  private Long userScore;
  private Long computerScore;

  public Game() {
  }

  public Game(
      @NotNull Long id,
      @NotNull String userChoice,
      @NotNull String computerChoice,
      @NotNull Long userScore,
      @NotNull Long computerScore) {
    this.id = id;
    this.userChoice = userChoice;
    this.computerChoice = computerChoice;
    this.userScore = userScore;
    this.computerScore = computerScore;
  }

  public Long getId() {
    return id;
  }

  public String getUserChoice() {
    return userChoice;
  }

  public String getComputerChoice() {
    return computerChoice;
  }

  public Long getUserScore() {
    return userScore;
  }

  public Long getComputerScore() {
    return computerScore;
  }

  @Override
  public String toString() {
    return "Game{" +
        "id=" + id +
        ", userChoice='" + userChoice + '\'' +
        ", computerChoice='" + computerChoice + '\'' +
        ", userScore=" + userScore +
        ", computerScore=" + computerScore +
        '}';
  }
}
```

#### The Repository

The repository allows us to CRUD the object in the DB.
Spring provides a lot of pre-configured methods here.

Create a new file `src/main/java/engineering/nxt/kotlin_everywhere/GameRepository.java`:

```{.java .numberLines}
package engineering.nxt.kotlin_everywhere;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface GameRepository extends CrudRepository<Game, Integer> {
  @Query("select sum(g.computerScore) from Game g")
  Optional<Long> computerScoreTotal();

  @Query("select sum(g.userScore) from Game g")
  Optional<Long> userScoreTotal();
}
```

#### The website

Our very basic website will be served by Spring.
We'll use the _Mustache_ template engine, which is very simple and therefore perfect for this showcase.

Create a new file `src/main/resources/templates/index.mustache`:

```{.html .numberLines}
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Rock, Paper, Scissors</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<h1>Rock, Paper, Scissors</h1>

<h2>Play</h2>

<div>
  <form method="post">
    <p>
      <button name="userChoice" value="Rock" type="submit">Rock</button>
      <button name="userChoice" value="Paper" type="submit">Paper</button>
      <button name="userChoice" value="Scissors" type="submit">Scissors</button>
    </p>
  </form>
</div>

<h2>Scoreboard</h2>

<div>
  <p>The computer won {{computerScoreTotal}} times.</p>
  <p>The player won {{userScoreTotal}} times.</p>
</div>

<h2>Previous Games</h2>

{{#entries}}
  <div>
    <h3>Game {{id}}</h3>
    <div>
      <p>The player chose {{userChoice}} and got {{userScore}} score points.</p>
      <p>The computer chose {{computerChoice}} and got {{computerScore}} score points.</p>
      <hr>
    </div>
  </div>
{{/entries}}
</body>

</html>
```

#### The Controller

Controllers in Spring are entry points for anything related to web requests.
We need the controller to deliver our website as well as for receiving a guestbook entry and adding it to the database.

Create a new file `src/main/java/engineering/nxt/kotlin_everywhere/IndexController.java`:

```{.java .numberLines}
package engineering.nxt.kotlin_everywhere;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class IndexController {
  private GameRepository gameRepository;

  public IndexController(@Autowired GameRepository gameRepository) {
    this.gameRepository = gameRepository;
  }

  @GetMapping
  public ModelAndView index(Map<String, Object> model) {
    model.put("entries", gameRepository.findAll());
    model.put("computerScoreTotal", gameRepository.computerScoreTotal().orElse(0L));
    model.put("userScoreTotal", gameRepository.userScoreTotal().orElse(0L));

    return new ModelAndView("index", model); // because `index.mustache`
  }

  @PostMapping
  public String play(@RequestParam String userChoice) {
    // this is a mock implementation for now
    Game g = new Game(null, userChoice, "Rock", 0L, 0L);
    gameRepository.save(g);

    return "redirect:/"; // redirect to `/`
  }
}
```

#### Start the App

You can start the application using gradle:

```bash
./gradlew bootRun
```

Then open a browser and point it to 'http://localhost:8080'.

### Mix Java With Kotlin

Now that we have a working _Java_ application, we want to see how simple it is to mix in _Kotlin_.

#### Add Kotlin Compiler

The Kotlin code needs to be compiled to Java bytecode.
Therefore we need to adjust the compile instructions.
During the build, the Java code and the Kotlin code will be converted to Java bytecode.

##### Gradle

Open the `build.gradle` file and add the following dependencies:

```patch
--- a/build.gradle
+++ b/build.gradle
@@ -1,7 +1,17 @@
+buildscript {
+  dependencies {
+    classpath "org.jetbrains.kotlin:kotlin-noarg:1.2.71"
+  }
+}
+
 plugins {
   id 'org.springframework.boot' version '2.1.8.RELEASE'
   id 'io.spring.dependency-management' version '1.0.8.RELEASE'
   id 'java'
+  id 'org.jetbrains.kotlin.jvm' version '1.2.71'
+  id 'org.jetbrains.kotlin.plugin.spring' version '1.2.71'
+  id 'org.jetbrains.kotlin.plugin.jpa' version '1.2.71'
+  id 'org.jetbrains.kotlin.plugin.noarg' version '1.2.71'
 }

 group = 'engineering.nxt'
@@ -23,7 +33,18 @@ dependencies {
   implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
   implementation 'org.springframework.boot:spring-boot-starter-mustache'
   implementation 'org.springframework.boot:spring-boot-starter-web'
+  implementation 'com.fasterxml.jackson.module:jackson-module-kotlin'
+  implementation 'org.jetbrains.kotlin:kotlin-reflect'
+  implementation 'org.jetbrains.kotlin:kotlin-stdlib-common'
+  implementation 'org.jetbrains.kotlin:kotlin-stdlib-jdk8'
   developmentOnly 'org.springframework.boot:spring-boot-devtools'
   runtimeOnly 'com.h2database:h2'
   testImplementation 'org.springframework.boot:spring-boot-starter-test'
 }
+
+compileKotlin {
+  kotlinOptions {
+    freeCompilerArgs += ['-Xjsr305=strict']
+    jvmTarget = '1.8'
+  }
+}
```

The change in the `plugins` section makes _Gradle_ recognize and build _Kotlin_ files.
It will henceforth compile any `.kt` files it finds in `src/main/java` and `src/main/kotlin`.

The changes in the `dependencies` section makes the [Kotlin Standard Library][kotlin_stdlib] available to all the code.
(Although it would be available to Java code as well, it's more useful to the Kotlin code.)
The [Kotlin Reflection Libraries][kotlin_reflection] are required for all libraries that use reflection.
As Spring relies heavily on it, that library must be added to our project.
Otherwise Spring would not be able to use reflection on Kotlin code.

[kotlin_stdlib]: https://kotlinlang.org/api/latest/jvm/stdlib/index.html
[kotlin_reflection]: https://kotlinlang.org/docs/reference/reflection.html

##### Maven

```patch
--- a/pom.xml
+++ b/pom.xml
@@ -16,6 +16,7 @@

 	<properties>
 		<java.version>1.8</java.version>
+		<kotlin.version>1.3.50</kotlin.version>
 	</properties>

 	<dependencies>
@@ -31,6 +32,18 @@
 			<groupId>org.springframework.boot</groupId>
 			<artifactId>spring-boot-starter-web</artifactId>
 		</dependency>
+		<dependency>
+			<groupId>com.fasterxml.jackson.module</groupId>
+			<artifactId>jackson-module-kotlin</artifactId>
+		</dependency>
+		<dependency>
+			<groupId>org.jetbrains.kotlin</groupId>
+			<artifactId>kotlin-reflect</artifactId>
+		</dependency>
+		<dependency>
+			<groupId>org.jetbrains.kotlin</groupId>
+			<artifactId>kotlin-stdlib-jdk8</artifactId>
+		</dependency>

 		<dependency>
 			<groupId>org.springframework.boot</groupId>
@@ -51,11 +64,38 @@
 	</dependencies>

 	<build>
+		<sourceDirectory>${project.basedir}/src/main/kotlin</sourceDirectory>
+		<testSourceDirectory>${project.basedir}/src/test/kotlin</testSourceDirectory>
 		<plugins>
 			<plugin>
 				<groupId>org.springframework.boot</groupId>
 				<artifactId>spring-boot-maven-plugin</artifactId>
 			</plugin>
+			<plugin>
+				<groupId>org.jetbrains.kotlin</groupId>
+				<artifactId>kotlin-maven-plugin</artifactId>
+				<configuration>
+					<compilerPlugins>
+						<plugin>jpa</plugin>
+						<plugin>spring</plugin>
+					</compilerPlugins>
+					<args>
+						<arg>-Xjsr305=strict</arg>
+					</args>
+				</configuration>
+				<dependencies>
+					<dependency>
+						<groupId>org.jetbrains.kotlin</groupId>
+						<artifactId>kotlin-maven-noarg</artifactId>
+						<version>${kotlin.version}</version>
+					</dependency>
+					<dependency>
+						<groupId>org.jetbrains.kotlin</groupId>
+						<artifactId>kotlin-maven-allopen</artifactId>
+						<version>${kotlin.version}</version>
+					</dependency>
+				</dependencies>
+			</plugin>
 		</plugins>
 	</build>
```

The changes in the `dependency` section may be a little bit surprising.
These changes do two things:
For one, they make the [Kotlin Standard Library][kotlin_stdlib] available in the code.
(It is more useful in Kotlin code, but it would be available to Java code as well.)

Second, the _Spring Framework_ relies on reflection.
But the Kotlin reflection works a little bit different that the Java reflection.
Therefore the [Kotlin Reflection Libraries][kotlin_reflection] are needed for operations that rely on reflection of Kotlin code.

The changes in the `plugin` section are just to make Maven aware of the Kotlin compiler.
The compiler does not even have to be installed locally.
Maven will just fetch it from the Maven repository.

##### Test the project

Although we have not added any Kotlin code yet, we should still be able to run the project:

```bash
./gradlew bootRun
```

> _Hint:_ If you're using _IntelliJ IDEA_ and it complains that Kotlin is not configured, then open the Gradle tool window and initiate a project sync.

#### Introducing Kotlin Code

Now that our build system is ready to compile Kotlin, let's add some Kotlin code, shall we?

In the previous chapter, you've created a Kotlin game that implements the logic for _Rock, Paper, Scissors_.

We won't be able to use the old `Game.kt` code exactly as it is, but we'll be able to use pieces of it.

And because our old `Game.kt` depended on an `enum` class with the name `Choice` (and we've already established that this was the better implementation than using plain `String`s), we'll implement this approach in our new implementation as well.

Create a new file `src/main/java/engineering/nxt/kotlin_everywhere/Choice.kt`. It will contain just the `enum class Choice` and it's choices:

```{.kotlin .numberLines}
package engineering.nxt.kotlin_everywhere

enum class Choice {
  Rock, Paper, Scissors
}
```

Then we'll adjust the `src/main/java/engineering/nxt/kotlin_everywhere/Game.java` file to use the `enum class Choice` instead of a `String`:

```patch
--- a/src/main/java/engineering/nxt/kotlin_everywhere/Game.java
+++ b/src/main/java/engineering/nxt/kotlin_everywhere/Game.java
@@ -10,8 +10,8 @@ public class Game {
   @Id
   @GeneratedValue
   private Long id;
-  private String userChoice;
-  private String computerChoice;
+  private Choice userChoice;
+  private Choice computerChoice;
   private Long userScore;
   private Long computerScore;

@@ -19,8 +19,9 @@ public class Game {
   }

   public Game(
       @NotNull Long id,
-      @NotNull String userChoice,
-      @NotNull String computerChoice,
+      @NotNull Choice userChoice,
+      @NotNull Choice computerChoice,
       @NotNull Long userScore,
       @NotNull Long computerScore) {
     this.userChoice = userChoice;
@@ -33,11 +34,11 @@ public class Game {
     return id;
   }

-  public String getUserChoice() {
+  public Choice getUserChoice() {
     return userChoice;
   }

-  public String getComputerChoice() {
+  public Choice getComputerChoice() {
     return computerChoice;
   }
```

The next step is to adjust the `/src/main/java/engineering/nxt/kotlin_everywhere/IndexController.java` file:

```patch
--- a/src/main/java/engineering/nxt/kotlin_everywhere/IndexController.java
+++ b/src/main/java/engineering/nxt/kotlin_everywhere/IndexController.java
@@ -28,9 +28,9 @@ public class IndexController {
   }

   @PostMapping
-  public String play(@RequestParam String userChoice) {
+  public String play(@RequestParam Choice userChoice) {
     // this is a mock implementation for now
-    Game g = new Game(null, userChoice, "Rock", 0L, 0L);
+    Game g = new Game(null, userChoice, Choice.Rock, 0L, 0L);
     gameRepository.save(g);

     return "redirect:/"; // redirect to `/`
```

If you now start the app, you will not notice a difference in the behavior.
But beneath it all we have mixed our first piece of Kotlin code with an 'existing' java application!

```bash
./gradlew bootRun
```

#### Implement our Game in Kotlin

Now that we've established with a simple example that the Kotlin and Java world work seamless together, we can add more complex Kotlin code.

We're going to implement the Game's logic in Kotlin.
And as I've promised, we'll be able to re-use the important parts of our previous `Game.kt` code.

We're going to need the `enum class Winner`.
Create the file `src/main/java/engineering/nxt/kotlin_everywhere/Winner.kt`:

```{.kotlin .numberLines}
package engineering.nxt.kotlin_everywhere

enum class Winner {
  NoOne, Computer, Player
}
```

Next we're going to create a new file that will eventually contain all the game's logic.
I therefore call it `Logic.kt`.

Create the file `src/main/java/engineering/nxt/kotlin_everywhere/GameLogic.kt`:

```{.kotlin .numberLines}
package engineering.nxt.kotlin_everywhere

class GameLogic {
}
```

The first functionality I want to use from the old game is the `fun computerDraw()`:

```{.kotlin .numberLines}
package engineering.nxt.kotlin_everywhere

class GameLogic {
  private fun computerDraw(): Choice {
    val choices = Choice.values()
    val computerChoice = choices.random()
    println("The computer has chosen $computerChoice.")

    return computerChoice
  }
}
```

Now, we need to adjust that function a little:

- We must remove the `private` keyword, as this method will later be called from code that is not part of the `GameLogic` class.
- The code on line 6 does not compile.
  This is because (at the time of writing) Spring 2.1 is built around Kotlin 1.2, and the `.random()` function was only introduced in Kotlin 1.3.
  We can replace the code using Java's `java.util.Random.nextInt()` like this:
  `val computerChoice = choices[Random().nextInt(choices.size)]`
- We can remove the line 7 as well because the `println` is not useful anymore.
- And since we now don't use the value `computerChoice` anymore, we can directly return it.

After these changes are applied, the code looks as follows:

```{.kotlin .numberLines}
package engineering.nxt.kotlin_everywhere

import java.util.*

class GameLogic {
  fun computerDraw(): Choice {
    val choices = Choice.values()

    // in Kotlin 1.3 this would be `choices.random()`
    return choices[Random().nextInt(choices.size)]
  }
}
```

Let's adopt the `IndexController.java` to make use of this function:

```{.java .numberLines}
package engineering.nxt.kotlin_everywhere;

// ... existing imports ...

@Controller
public class IndexController {
  // ... existing code ...

  @PostMapping
  public String play(@RequestParam Choice userChoice) {
    // this is a mock implementation for now
    GameLogic gameLogic = new GameLogic();
    Choice computerChoice = gameLogic.computerDraw();

    Game g = new Game(null, userChoice, computerChoice, 0L, 0L);
    gameRepository.save(g);

    return "redirect:/"; // redirect to `/`
  }
}
```

If you run the application now (`./gradlew bootRun`), you'll see that the computer's choice is now indeed a random choice, and not `Rock` all the time.

The logic next step is to add the evaluation logic to our game.
In the previous code this was called `fun referee(...)`.
We're going to copy this function plus the three functions `fun rockGiven(...)`, `fun paperGiven(...)` and `fun scissorsGiven(...)` to our `GameLogic.kt` class:

```{.kotlin .numberLines}
package engineering.nxt.kotlin_everywhere

import java.util.*

class GameLogic {
  fun computerDraw(): Choice {
    val choices = Choice.values()

    // in Kotlin 1.3 this would be `choices.random()`
    return choices[Random().nextInt(choices.size)]
  }

  fun referee(userChoice: Choice, computerChoice: Choice): Winner {
    return when (userChoice) {
      Choice.Rock -> rockGiven(computerChoice)
      Choice.Paper -> paperGiven(computerChoice)
      Choice.Scissors -> scissorsGiven(computerChoice)
    }
  }

  private fun rockGiven(computerChoice: Choice): Winner {
    return when (computerChoice) {
      Choice.Rock -> Winner.NoOne
      Choice.Paper -> Winner.Computer
      Choice.Scissors -> Winner.Player
    }
  }

  private fun paperGiven(computerChoice: Choice): Winner {
    return when (computerChoice) {
      Choice.Rock -> Winner.Player
      Choice.Paper -> Winner.NoOne
      Choice.Scissors -> Winner.Computer
    }
  }

  private fun scissorsGiven(computerChoice: Choice): Winner {
    return when (computerChoice) {
      Choice.Rock -> Winner.Computer
      Choice.Paper -> Winner.Player
      Choice.Scissors -> Winner.NoOne
    }
  }
}
```

After inserting `fun referee(...)` I have removed the irrelevant parts:

- Everything in `val result = when (winner) { ... }` in the old code is not relevant anymore, because we have a different logic to announce the winner and keep track of the score.
- The same is true for the `println` function.
- The function's signature has changed:
  - It's no longer `private`, because we'll need to call the code from places outside of of the `GameLogic` class.
  - It has a return type (`Winner`), because it will now directly return who's the winner.

After those changes, we are finally able to use this code in the `IndexController.java` and complete the game's implementation:

```{.java .numberLines}
package engineering.nxt.kotlin_everywhere;

// ... existing imports ...

@Controller
public class IndexController {
  // ... existing code ...

  @PostMapping
  public String play(@RequestParam Choice userChoice) {
    GameLogic gameLogic = new GameLogic();
    Choice computerChoice = gameLogic.computerDraw();

    long userScore = 0L;
    long computerScore = 0L;

    Winner winner = gameLogic.referee(userChoice, computerChoice);
    switch(winner) {
      case Player:
        userScore = 1L;
        break;
      case Computer:
        computerScore = 1L;
        break;
      default:
        // the score does not change
    }

    Game g = new Game(null, userChoice, computerChoice, userScore, computerScore);
    gameRepository.save(g);

    return "redirect:/"; // redirect to `/`
  }
}
```

#### Start the App

We can now try the app again:

```bash
./gradlew bootRun
```

Open your browser and go to `http://localhost:8080/`.
This rudimentary game should now work as expected.

### Convert Java to Kotlin

> **Important:** The next example requires [IntelliJ IDEA (Community Edition or Ultimate Edition)][intellij].

[intellij]: https://www.jetbrains.com/idea/download/index.html

#### Import the Project in IntelliJ

After starting _IntelliJ_, choose _Open_.
Navigate to the root folder of the project.
(This is the folder that contains the `build.gradle` file.)

In the dialog _Import Project from Gradle_ the following options are recommended:

* Use auto-import
* Create directories for empty content roots automatically
* Use default Gradle wrapper
* Use Project JDK

After you click _Ok_, IntelliJ will start to load and compile your project.

#### Convert A File

Now open the file `src/main/java/engineering/nxt/kotlin_everywhere/Game.java` in IntelliJ.

Open the _Code_ menu and select _Convert Java File to Kotlin File_.

This will get you pretty far already:

```{.kotlin .numberLines}
package engineering.nxt.kotlin_everywhere

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.validation.constraints.NotNull

@Entity
class Game {
  @Id
  @GeneratedValue
  val id: Int? = null
  val userChoice: Choice
  val computerChoice: Choice
  val userScore: Long?
  val computerScore: Long?

  constructor() {}

  constructor(
      @NotNull id: Int?,
      @NotNull userChoice: Choice,
      @NotNull computerChoice: Choice,
      @NotNull userScore: Long?,
      @NotNull computerScore: Long?) {
    this.id = id
    this.userChoice = userChoice
    this.computerChoice = computerChoice
    this.userScore = userScore
    this.computerScore = computerScore
  }

  override fun toString(): String {
    return "Game{" +
        "id=" + id +
        ", userChoice='" + userChoice + '\''.toString() +
        ", computerChoice='" + computerChoice + '\''.toString() +
        ", userScore=" + userScore +
        ", computerScore=" + computerScore +
        '}'.toString()
  }
}
```

The IDE will complain on lines 13 to 16 that the _Property must be initialized or be abstract_.
This is because the code converter created an explicit empty constructor on line 18 for us.
We can remove it.

Now that the code compiles you can verify that the application still works by running it: `./gradlew bootRun`.

#### Apply Kotlin Idioms

The code converter gets us pretty far already, especially if you want to convert a big project from Java to Kotlin.
But Kotlin allows a more concise syntax.
So we can reduce the above code and the result will be something like this:

```kotlin
package engineering.nxt.kotlin_everywhere

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Game(
    @Id
    @GeneratedValue
    val id: Int? = null,
    val userChoice: Choice,
    val computerChoice: Choice,
    val userScore: Long,
    val computerScore: Long)
```

This is a `data class`.
Data classes are like regular classes.
But they are an optimization for code that only contains data and not (much) functionality.
In the Java world, such a class would be called a _POJO_, which stands for _Plain Old Java Object_.

A `data class` has some interesting benefits:
All setters and getters are automatically generated.
Also the `toString()` method is automatically implemented in a meaningful way.
The same is true for `equals()`, `hashCode()` and more.
For a full description and all the details, I advise you [to read the Kotlin documentation about the `data class`][data-class]

[data-class]: https://kotlinlang.org/docs/reference/data-classes.html

Other than that, nullability is automatically inferred from and enforced by the type system.
(I.e. the `id` can be `null` (when the entity is not yet persisted) and therefore the `id`'s type is `Int?`.)

And of course, _setter_ methods are only generated for `var` fields, and omitted for `val` fields.

You should now try the final app again:

```bash
./gradlew bootRun
```

## Ideas for more

If you're here, you've made it through all the exercises that I've prepared.
Here are some ideas to go from here:

- Make the methods of the `GameLogic` class static.
  And make sure, that they are accessible from the Java world as static methods.
  Your keywords for this are `companion object` and `@JvmStatic`.
- Convert all remaining Java code to Kotlin.
- Open the `Game.kt` file from Chapter 2 in IntelliJ.
  It will offer you some suggestions to simplify the code.
  Apply them and see how concise Kotlin can be.
- Go back to https://start.spring.io, but this time generate a Kotlin project.
  Inspect the `build.gradle.ks` file.
  What is different?
  Which Gradle-style do you prefer, Kotlin or Groovy?
- _Not specific to Kotlin:_ Make the website look nicer.
- _Complex task_: Enhance the game in such a way that two human players can play against each other from two computers. (You can start by using `static` variables in the `IndexController` for this before implementing it using a database.)
